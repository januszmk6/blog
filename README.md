#Installation

Run following commands to install application:
````
docker-compose up
docker-compose exec --user $(id -u):$(id -g) -w /var/www/ php-fpm php composer.phar install
cp app/env.example app/.env
docker-compose exec --user $(id -u):$(id -g) -w /var/www/ php-fpm php artisan key:generate
docker-compose exec --user $(id -u):$(id -g) -w /var/www/ php-fpm php artisan jwt:secret
npm install
npm run dev
docker-compose exec --user $(id -u):$(id -g) -w /var/www/ php-fpm php artisan migrate:fresh
docker-compose exec --user $(id -u):$(id -g) -w /var/www/ php-fpm php artisan migrate --seed
````
Application can be accessed at: http://localhost:8321/.
Demo user credentials are: 
````
email: admin@test.com
password: admin
````

In case of permissions issue, fix permissions to app/storage, e.g.:
`sudo chown -R $(id -u):$(id -g) storage`
`sudo chmod -R 777 storage`

#
To see api specification, run:
````
docker-compose exec --user $(id -u):$(id -g) -w /var/www/ php-fpm php vendor/bin/openapi app/
````
To run tests, execute this command: 
````
docker-compose exec --user $(id -u):$(id -g) -w /var/www/ php-fpm php vendor/bin/phpunit 
````
