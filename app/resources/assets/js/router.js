import VueRouter from 'vue-router' // Pages
import Home from './pages/Home'
import Login from './pages/Login'
import Create from './pages/Admin/Create'
import Update from './pages/Admin/Update'
import View from './pages/View'
import Search from './pages/Search'

const routes = [
    {
        path: '/',
        name: 'home',
        component: Home,
        meta: {
            auth: undefined
        }
    },
    {
        path: '/search',
        name: 'search',
        component: Search,
        meta: {
            auth: undefined
        }
    },
    {
        path: '/login',
        name: 'login',
        component: Login,
        meta: {
            auth: false
        }
    },
    {
        path: '/view/:slug',
        name: 'view',
        component: View,
        props: true,
        meta: {
            auth: undefined
        }
    },
    {
        path: '/admin/create',
        name: 'create',
        component: Create,
        meta: {
            auth: true
        }
    },
    {
        path: '/admin/update/:slug',
        name: 'update',
        component: Update,
        props: true,
        meta: {
            auth: true,
        }
    },
];
const router = new VueRouter({
    history: true,
    mode: 'history',
    routes,
});
export default router
