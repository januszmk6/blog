<?php
namespace Tests\Unit;

use App\Http\Controllers\BlogController;
use App\Http\Requests\PostSearchRequest;
use App\Post;
use Illuminate\Pagination\LengthAwarePaginator;
use Tests\TestCase;

class BlogControllerTest extends TestCase
{
    public function testSearch()
    {
        $paginator = new LengthAwarePaginator([
            [
                'title' => 'test'
            ],
            [
                'title' => 'test2',
            ],
        ], 2, 10);
        $model = \Mockery::mock(Post::class);
        $model
            ->shouldIgnoreMissing($model)
            ->shouldReceive('paginate')
            ->withNoArgs()
            ->once()
            ->andReturn($paginator);

        $postSearchRequest = \Mockery::mock(PostSearchRequest::class);
        $postSearchRequest
            ->shouldReceive('validated')
            ->withNoArgs()
            ->once()
            ->andReturn([
                'query' => 'test',
            ]);

        $controller = new BlogController($model);
        $result = $controller->search($postSearchRequest);

        $this->assertEquals('success', $result['status']);
        $this->assertEquals($paginator, $result['data']);
    }
}
