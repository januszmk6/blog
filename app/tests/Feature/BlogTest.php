<?php

namespace Tests\Feature;

use App\Post;
use Faker\Factory;
use Tests\TestCase;

class BlogTest extends TestCase
{
    private $posts = [];

    public function setUp()
    {
        parent::setUp();

        $db = \DB::connection('mongodb');
        $db->drop(['post']);
        $collection = $db->selectCollection('post');
        $collection->createIndexes([['key' => ['title' => 1]], ['key' => ['slug' => 1]]]);

    }

    private function createRandomPosts()
    {
        $faker = Factory::create();

        for ($i = 0; $i < 100; $i++) {
            $post = new Post([
                'title'   => $faker->sentence(),
                'content' => $faker->paragraph(10, true),
            ]);
            $post->save();
            $this->posts[] = $post;
        }
    }

    public function testListingPosts()
    {
        $this->createRandomPosts();
        $response = $this->get('/api/blog/posts?page=2');

        $response->assertStatus(200);
        $data = json_decode($response->getContent(), true, JSON_THROW_ON_ERROR);
        $this->assertEquals('success', $data['status']);
        $this->assertIsArray($data['data']);
        $this->assertEquals(100, $data['data']['total']);
        $this->assertEquals(30, $data['data']['to']);
        $this->assertEquals(15, $data['data']['per_page']);
        $this->assertCount(15, $data['data']['data']);
    }

    /**
     * @test
     */
    public function listingOutsideOfRangeShouldReturnNoData()
    {
        $this->createRandomPosts();

        $response = $this->get('/api/blog/posts?page=30');

        $response->assertStatus(200);
        $data = json_decode($response->getContent(), true, JSON_THROW_ON_ERROR);
        $this->assertEquals('success', $data['status']);
        $this->assertIsArray($data['data']);
        $this->assertCount(0, $data['data']['data']);
        $this->assertEquals(100, $data['data']['total']);
    }

    /**
     * @test
     */
    public function searchingShouldReturnPostsResult()
    {
        $faker = Factory::create();
        for ($i = 0; $i < 10; $i++) {
            $post = new Post([
                'title' => 'Test' . $faker->sentence(),
                'content' => $faker->paragraph(8, true),
            ]);
            $post->save();
        }

        for ($i = 0; $i < 15; $i++) {
            $post = new Post([
                'title' => $faker->randomAscii,
                'content' => $faker->paragraph(8, true),
            ]);
            $post->save();
        }

        $response = $this->get('/api/blog/search?query=test');

        $response->assertStatus(200);
        $data = json_decode($response->getContent(), true, JSON_THROW_ON_ERROR);
        $this->assertEquals('success', $data['status']);
        $this->assertCount(10, $data['data']['data']);
        $this->assertEquals(10, $data['data']['total']);
    }

    /**
     * @test
     */
    public function viewingPostShouldReturnPostData()
    {
        $this->createRandomPosts();
        $post = $this->posts[0];
        $response = $this->get(sprintf('/api/blog/view/%s', $post->slug));
        $response->assertStatus(200);
        $data = json_decode($response->getContent(), true, JSON_THROW_ON_ERROR);
        $this->assertEquals('success', $data['status']);
        $this->assertEquals($post->title, $data['data']['title']);
        $this->assertEquals($post->content, $data['data']['content']);
    }

    /**
     * @test
     */
    public function viewingNotExistingPostShouldReturnError()
    {
        $this->createRandomPosts();
        $response = $this->get('/api/blog/view/test-slug');
        $response->assertStatus(404);
        $data = json_decode($response->getContent(), true, JSON_THROW_ON_ERROR);
        $this->assertEquals('error', $data['status']);
        $this->assertEquals('Object not found', $data['message']);
    }
}
