<?php

use App\User;
use Illuminate\Database\Seeder;
use App\Post;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'     => 'Admin',
            'email'    => 'admin@test.com',
            'password' => Hash::make('admin'),
        ]);
        $faker = Faker\Factory::create();

        for ($i = 0; $i < 100; $i++) {
            $post = new Post([
                'title' => $faker->sentence(),
                'content' => $faker->paragraph(10, true),
            ]);
            $post->save();
        }
    }
}
