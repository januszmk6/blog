<?php
namespace App\Http\Requests;

class PostUpdateRequest extends PostAddRequest
{
    public function rules(): array
    {
        $rules = parent::rules();

        $rules['slug'] = 'required|string|min:3';

        return $rules;
    }
}
