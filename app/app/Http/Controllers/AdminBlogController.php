<?php
namespace App\Http\Controllers;

use App\Http\Requests\PostAddRequest;
use App\Http\Requests\PostDeleteRequest;
use App\Http\Requests\PostUpdateRequest;
use App\Post;
use App\Repository\PostRepository;
use OpenApi\Annotations as OA;

class AdminBlogController extends Controller
{
    private $postRepository;

    public function __construct(Post $model)
    {
        $this->postRepository = new PostRepository($model);
    }

    /**
     * @OA\Post(
     *     path="/admin/post",
     *     summary="Save new post",
     *     @OA\RequestBody(
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(
     *                  required={"title", "content"},
     *                  @OA\Property(
     *                      property="title",
     *                      type="string",
     *                      minLength=3
     *                  ),
     *                  @OA\Property(
     *                      property="content",
     *                      type="string",
     *                      minLength=10
     *                  ),
     *              )
     *          )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="OK",
     *     ),
     *     @OA\Response(
     *          response="422",
     *          description="Validation error",
     *     )
     * )
     *
     * @param PostAddRequest $postManageRequest
     * @return array
     */
    public function store(PostAddRequest $postManageRequest): array
    {
        $this->postRepository->create($postManageRequest->validated());

        return [
            'status' => 'success',
        ];
    }

    /**
     * @OA\Put(
     *     path="/admin/post",
     *     summary="Updates existing post",
     *     @OA\RequestBody(
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(
     *                  required={"title", "content", "slug"},
     *                  @OA\Property(
     *                      property="title",
     *                      type="string",
     *                      minLength=3
     *                  ),
     *                  @OA\Property(
     *                      property="content",
     *                      type="string",
     *                      minLength=10
     *                  ),
     *                  @OA\Property(
     *                      property="slug",
     *                      type="string",
     *                      minLength=3
     *                  )
     *              )
     *          )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="OK",
     *     ),
     *     @OA\Response(
     *          response="422",
     *          description="Validation error",
     *     ),
     *     @OA\Response(
     *          response="404",
     *          description="not found",
     *     )
     * )
     *
     * @param PostUpdateRequest $postManageRequest
     * @return array
     */
    public function update(PostUpdateRequest $postManageRequest): array
    {
        $values = $postManageRequest->validated();
        $slug = $values['slug'];
        unset($values['slug']);
        $this->postRepository->update($slug, $values);

        return [
            'status' => 'success',
        ];
    }

    /**
     * @OA\Delete(
     *     path="/admin/post",
     *     summary="Deletes existing post",
     *     @OA\RequestBody(
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(
     *                  required={"slug"},
     *                  @OA\Property(
     *                      property="slug",
     *                      type="string",
     *                      minLength=3
     *                  )
     *              )
     *          )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="OK",
     *     ),
     *     @OA\Response(
     *          response="422",
     *          description="Validation error",
     *     ),
     *     @OA\Response(
     *          response="404",
     *          description="not found",
     *     )
     * )
     *
     * @param PostDeleteRequest $postDeleteRequest
     * @return array
     */
    public function delete(PostDeleteRequest $postDeleteRequest): array
    {
        $values = $postDeleteRequest->validated();
        $this->postRepository->delete($values['slug']);

        return [
            'status' => 'success',
        ];
    }
}
