<?php
namespace App\Http\Controllers;

use App\User;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Auth\AuthManager;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AuthController extends Controller
{
    private $authManager;

    private $guard;

    private $model;

    public function __construct(
        AuthManager $authManager,
        Guard $guard,
        User $model
    )
    {
        $this->authManager = $authManager;
        $this->guard = $guard;
        $this->model = $model;
    }

    public function login(Request $request): Response
    {
        $credentials = $request->only('email', 'password');
        if ($token = $this->authManager->guard()->attempt($credentials)) {
            return new Response([
                'status' => 'success',
            ], 200, [
                'Authorization' => $token,
            ]);
        }
        throw new AuthenticationException();
    }

    public function user(): array
    {
        $user = $this->model->find($this->guard->user()->id);

        return [
            'status' => 'success',
            'data'   => $user,
        ];
    }

    public function logout(): array
    {
        $this->authManager->guard()->logout();

        return [
            'status' => 'success',
            'msg'    => 'Logged out Successfully.',
        ];
    }

    public function refresh(): Response
    {
        if ($token = $this->authManager->guard()->refresh()) {
            return new Response([
                'status' => 'success',
            ], 200, [
                'Authorization' => $token,
            ]);
        }

        throw new AuthenticationException();
    }
}
