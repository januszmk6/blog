<?php
namespace App\Http\Controllers;

use App\Exceptions\PostNotFoundException;
use App\Http\Requests\PostSearchRequest;
use App\Post;
use App\Repository\PostRepository;
use OpenApi\Annotations as OA;

/**
 * @OA\Info(
 *     version="1.0",
 *     title="Blog API",
 * )
 */
class BlogController extends Controller
{
    private $postRepository;

    public function __construct(Post $model)
    {
        $this->postRepository = new PostRepository($model);
    }

    /**
     * @OA\Get(
     *     path="/blog/posts",
     *     summary="Listing posts",
     *     @OA\Parameter(
     *          name="page",
     *          in="query",
     *          @OA\Schema(
     *              type="integer",
     *          )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="success"
     *     )
     * )
     *
     * @return array
     */
    public function index(): array
    {
        $posts = $this->postRepository->getAllPaginator();

        return [
            'status' => 'success',
            'data'   => $posts,
        ];
    }

    /**
     * @OA\Get(
     *     path="/blog/search",
     *     summary="Listing posts",
     *     @OA\Parameter(
     *          name="page",
     *          in="query",
     *          @OA\Schema(
     *              type="integer",
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="query",
     *          in="query",
     *          required=true,
     *          @OA\Schema(
     *              type="string",
     *              minLength=3,
     *          )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="success"
     *     )
     * )
     *
     * @param PostSearchRequest $request
     * @return array
     */
    public function search(PostSearchRequest $request): array
    {
        $posts = $this->postRepository->getSearchPaginator($request->validated());

        return [
            'status' => 'success',
            'data'   => $posts,
        ];
    }

    /**
     * @OA\Get(
     *     path="/blog/view/{slug}",
     *     summary="Listing posts",
     *     @OA\Parameter(
     *          name="slug",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="string",
     *          )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="success"
     *     ),
     *     @OA\Response(
     *          response="404",
     *          description="not found",
     *     )
     * )
     *
     * @param string $slug
     * @return array
     * @throws PostNotFoundException
     */
    public function view(string $slug): array
    {
        $post = $this->postRepository->find($slug);

        return [
            'status' => 'success',
            'data'   => $post,
        ];
    }
}
