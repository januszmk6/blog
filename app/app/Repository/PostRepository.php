<?php
namespace App\Repository;

use App\Exceptions\PostNotFoundException;
use App\Post;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class PostRepository
{
    private $model;

    public function __construct(Post $model)
    {
        $this->model = $model;
    }

    public function getSearchPaginator(array $criteria): LengthAwarePaginator
    {
        if (isset($criteria['query'])) {
            $pagination = $this->model
                ->where('title', 'LIKE', '%' . $criteria['query'] . '%')
                ->orderBy('updated_at', 'DESC')
                ->paginate();
            $pagination->appends('query', $criteria['query']);

            return $pagination;
        }

        return $this->model->paginate();
    }

    public function getAllPaginator(): LengthAwarePaginator
    {
        return $this->model
            ->orderBy('updated_at', 'DESC')
            ->paginate();
    }

    public function create(array $values): void
    {
        $post = new Post($values);
        $post->save();
    }

    public function update(string $slug, array $values): void
    {
        $post = $this->find($slug);

        $post->update($values);
        $post->save();
    }

    public function delete(string $slug): void
    {
        $post = $this->find($slug);

        $post->delete();
    }

    public function find(string $slug): Post
    {
        $post = $this->model->where('slug', '=', $slug)->first();
        if ($post === null) {
            throw new PostNotFoundException();
        }

        return $post;
    }
}
